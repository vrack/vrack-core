module.exports = {

  /**
   *      PLUGINS
   **/
  Broadcaster: require('./src/plugins/Broadcaster'),
  ConverterJS: require('./src/plugins/ConverterJS'),
  Dashboard: require('./src/plugins/dashboard/Dashboard'),
  DashboardMaster: require('./src/plugins/DashboardMaster'),
  DashboardStorage: require('./src/plugins/DashboardStorage'),
  DashboardsManager: require('./src/plugins/DashboardsManager'),
  DashboardsWatcher: require('./src/plugins/DashboardsWatcher'),
  DevicesManager: require('./src/plugins/DevicesManager'),
  DevicesInformation: require('./src/plugins/DevicesInformation'),
  Guard: require('./src/plugins/Guard'),
  ApiKeyManager: require('./src/plugins/KeyManager'),
  Master: require('./src/plugins/Master'),
  WebSocketServer: require('./src/plugins/WebSocketServer'),
  WorkerProvider: require('./src/plugins/WorkerProvider'),
  WorkersManager: require('./src/plugins/WorkersManager'),
  SystemEvents: require('./src/plugins/SystemEvents'),
  System: require('./src/plugins/System'),

  /**
   *      ERRORS
   **/
  CommandError: require('./src/errors/CommandError'),
  DashboardError: require('./src/errors/DashboardError'),
  DeviceError: require('./src/errors/DeviceError'),
  Error: require('./src/errors/Error'),
  ListenError: require('./src/errors/ListenError'),
  ValidationError: require('./src/errors/ValidationError'),

  /**
   *      EXTENDS
   **/
  Device: require('./src/plugins/dashboard/Device'),
  Port: require('./src/plugins/dashboard/Port'),
  Action: require('./src/plugins/dashboard/Action'),

  /**
   *      HELPERS
   **/
  Rule: require('./src/helpers/validator/Rule'),
  Validator: require('./src/helpers/validator/Validator'),
  Utility: require('./src/helpers/Utility')
}
