module.exports = class extends Error {
  constructor (message) {
    super(message)
    this.name = 'VrackError'
    this.action = null
    this.vrackError = true
  }

  import (error) {
    for (const key of Object.getOwnPropertyNames(error)) this[key] = error[key]
    return this
  }

  export () {
    const result = {}
    for (const key of Object.getOwnPropertyNames(this)) result[key] = this[key]
    return result
  }

  static make (error) {
    const nerror = new this(error.message)
    return nerror.import(error)
  }

  static objectify (error) {
    const result = {}
    for (const key of Object.getOwnPropertyNames(error)) result[key] = error[key]
    return result
  }
}
