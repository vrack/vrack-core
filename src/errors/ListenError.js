const VRackError = require('./Error')

module.exports = class extends VRackError {
  constructor (message) {
    super(message)
    this.name = 'ListenError'
  }
}
