
function camelize (text) {
  return text.replace(/^([A-Z])|[.]+(\w)/g, function (match, p1, p2, offset) {
    if (p2) return p2.toUpperCase()
    return p1.toLowerCase()
  })
}

function validJSON (text) {
  try {
    const cfg = JSON.parse(text)
    if (!(cfg && typeof cfg === 'object')) return false
    return cfg
  } catch (e) {
    return false
  }
}

function toPath (text) {
  return text.toLowerCase()
}

module.exports = {
  camelize, validJSON, toPath
}
