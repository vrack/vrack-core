module.exports = class {
  #settings = {
    default: undefined,
    require: false,
    is: [],
    expressions: [],
    customs: [],
    enum: [],
    path: '',
    example: undefined,
    description: undefined,
    message: ''
  }

  #list = [
    'default',
    'require',
    'expressions',
    'is',
    'customs',
    'enum',
    'path',
    'example',
    'description',
    'message'
  ]

  #safeList = [
    'default',
    'require',
    'expressions',
    'is',
    'enum',
    'path',
    'example',
    'description',
    'message'
  ]

  constructor (path) {
    this.#settings.path = path
  }

  default (def) {
    this.#settings.default = def
    return this
  }

  required () {
    this.#settings.require = true
    return this
  }

  expression (exp) {
    this.#settings.expressions.push(exp)
    return this
  }

  custom (name, cb) {
    this.#settings.customs.push({ name, cb })
    return this
  }

  enum (arr) {
    this.#settings.enum = arr
    return this
  }

  message (message) {
    this.#settings.message = message
    return this
  }

  isArray () {
    this.#settings.is.push({ type: 'array', typeSource: 'Array' })
    return this
  }

  isInteger () {
    this.#settings.is.push({ type: 'integer', typeSource: 'Number' })
    return this
  }

  isNumber () {
    this.#settings.is.push({ type: 'number', typeSource: 'Typeof' })
    return this
  }

  isString () {
    this.#settings.is.push({ type: 'string', typeSource: 'Typeof' })
    return this
  }

  isObject () {
    this.#settings.is.push({ type: 'object', typeSource: 'Typeof' })
    return this
  }

  isBoolean () {
    this.#settings.is.push({ type: 'boolean', typeSource: 'Typeof' })
    return this
  }

  isFile () {
    this.#settings.is.push({ type: 'file', typeSource: 'filesystem' })
    return this
  }

  isDirectory () {
    this.#settings.is.push({ type: 'directory', typeSource: 'filesystem' })
    return this
  }

  example (value) {
    this.#settings.example = value
    return this
  }

  description (value) {
    this.#settings.description = value
    return this
  }

  export (safe) {
    const res = {}
    var list = []
    if (safe) list = this.#safeList; else list = this.#list
    for (const prop of list) res[prop] = this.#settings[prop]
    return res
  }
}
