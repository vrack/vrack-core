const vm = require('vm')
const fs = require('fs')
const path = require('path')
const util = require('util')
module.exports = class {
  constructor () {
    this.context = { result: false, value: false }
    vm.createContext(this.context)
  }

  operations = ['checkDefault', 'checkRequire', 'checkIs', 'checkEnum', 'checkExpression', 'checkCustom']

  problems = {}

  validate (rules, data, useExample = false) {
    if (rules !== undefined && !Array.isArray(rules)) throw new Error('Rules must by array of rules')
    this.rules = rules
    this.data = data
    this.problems = {}
    var valid = true
    for (const rule of this.rules) {
      const exRule = rule.export(false)
      var sd = this.getFromPath(exRule.path, this.data)
      for (const operation of this.operations) {
        const result = this[operation](exRule, sd.sdata, sd.act, useExample)
        if (result !== undefined && result.length) {
          if (!this.problems[exRule.path]) this.problems[exRule.path] = []
          if (exRule.message) { this.problems[exRule.path].push(this.makeMessage(exRule.message, exRule, sd.sdata, sd.act)); continue }
          for (const problem of result) {
            this.problems[exRule.path].push(problem)
            valid = false
          }
        }
      }
    }
    return valid
  }

  toString () {
    var result = ''
    for (const path in this.problems) result += `${path} => ${this.problems[path].join(', ')} `
    return result
  }

  checkDefault (rule, sdata, act, useExample) {
    if (sdata[act] === undefined &&
      rule.default !== undefined
    ) sdata[act] = rule.default
    if (sdata[act] === undefined &&
      rule.default === undefined &&
      useExample
    ) sdata[act] = rule.example
  }

  checkRequire (rule, sdata, act) {
    if (sdata[act] === undefined && rule.require) return ['The value must be defined (required param)']
  }

  checkExpression (rule, sdata, act) {
    if (sdata[act] === undefined) return []
    const expressionProblems = []
    for (const expression of rule.expressions) {
      this.context.result = false
      this.context.value = sdata[act]
      try {
        vm.runInContext(`result = (${expression})`, this.context)
        if (!this.context.result) expressionProblems.push(`Violation of rule (${expression})`)
      } catch (err) {
        expressionProblems.push(`Invalid expression (${expression})`)
      }
    }
    return expressionProblems
  }

  checkIs (rule, sdata, act, useExample) {
    if (sdata[act] === undefined) return []
    const isProblems = []
    for (const isRule of rule.is) if (!this.is(sdata[act], isRule.type, useExample)) isProblems.push(isRule.type)
    if (isProblems.length > 0 && isProblems.length === rule.is.length) {
      const first = isProblems.shift()
      return [`The value must be of type ${first}` + isProblems.join(' or ')]
    }
  }

  checkEnum (rule, sdata, act) {
    if (sdata[act] === undefined) return []
    if (!rule.enum.length) return []
    if (rule.enum.indexOf(sdata[act]) === -1) return ['The parameter must have one following value (' + rule.enum.join(', ') + ')']
  }

  checkCustom (rule, sdata, act) {
    if (sdata[act] === undefined) return []
    const customProblems = []
    for (const custom of rule.customs) {
      try {
        if (!custom.cb(sdata[act])) customProblems.push(custom.name)
      } catch (err) {
        customProblems.push('Custom error: ' + err.toString())
      }
    }
    return customProblems
  }

  makeError (problem) {
    return `Error validation path '${problem.rule.path}' - ${problem.message} \n`
  }

  is (value, type, useExample) {
    switch (type) {
      case 'array':
        return Array.isArray(value)
      case 'integer':
        return Number.isInteger(value)
      case 'number':
        return (typeof value === 'number')
      case 'string':
        return (typeof value === 'string')
      case 'object':
        return (typeof value === 'object')
      case 'boolean':
        return (typeof value === 'boolean')
      case 'file':
        if (useExample) return true
        if (!fs.existsSync(path.join(value))) return false
        return fs.lstatSync(path.join(value)).isFile()
      case 'directory':
        if (useExample) return true
        if (!fs.existsSync(path.join(value))) return false
        return fs.lstatSync(path.join(value)).isDirectory()
    }
  }

  /**
   * 
   * @param {string} message 
  */
  makeMessage(message, rule, sdata, act){
    let value = sdata[act]
    let example = rule.example
    let def = rule.default
    if (typeof value === "object" ) value = util.inspect(value, { showHidden: false, depth: null, compact: false })
    if (typeof rule.example === "object" || typeof rule.example === "function") example = util.inspect(example, { showHidden: false, depth: null, compact: false })
    if (typeof rule.default === "object" || typeof rule.default === "function") def = util.inspect(def, { showHidden: false, depth: null, compact: false })
    message = message.replace('{value}', value)
    message = message.replace('{description}', rule.description)
    message = message.replace('{default}', def)
    if (example) message = message.replace('{example}', example)
    return message
  }

  getFromPath (path, data) {
    var acts = path.split('.')
    var sdata = data
    var i = 1
    for (var act of acts) {
      if (sdata === undefined) throw new Error('Path ' + path + ' link to undefined var')
      if (acts.length === i) return { sdata, act }; else sdata = sdata[act]; i++
    }
  }
}
