const ValidatorClass = require('../helpers/validator/Validator')
const Validator = new ValidatorClass()
module.exports = class {
  config = {}
  checkParams () {
    return []
  }

  validate () {
    if (!Validator.validate(this.checkParams(), this.config)) {
      throw new Error(`Plugin config validation errors: ${Validator.toString()}`)
    }
  }

  init () {}
  process () {}
}
