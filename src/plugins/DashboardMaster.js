const Loader = require('vrack-loader')

const ValidatorClass = require('../helpers/validator/Validator')
const Validator = new ValidatorClass()

const VRackUtility = require('../helpers/Utility')
const DeviceError = require('../errors/DeviceError')
const CommandError = require('../errors/CommandError')
const ListenError = require('../errors/ListenError')
const Rule = require('../helpers/validator/Rule')
const util = require('util')

module.exports = class {
  constructor () {
    this.commands = {}
    this.minRenderDelay = 200

    this.broadcastTypes = {
      broadcastTerminal: 'terminal',
      broadcastError: 'error',
      broadcastNotify: 'notify',
      broadcastAlert: 'alert',
      broadcastEvent: 'event'
    }

    this._dashboard = {}
    this._renderTimer = false
    this._renderPull = new Set()
    this._renderAccess = new Set()
    this._ListenPull = new Map()
  }

  init () {
    this._dashboard = Loader.get('Dashboard')
    this.Provider = Loader.get('WorkerProvider')

    this._dashboard.on('broadcastRender', (deviceID) => {
      this._renderPull.add(deviceID)
      this._renderAccess.add(deviceID)
      if (!this._renderTimer) this._render()
    })

    for (const type in this.broadcastTypes) {
      this._dashboard.on(type, (device, message = '', trace = '') => {
        this.Provider.broadcast(
          this.#makeBroadcastChannel(device, this.broadcastTypes[type]),
          { message, trace: this.#makeBroadcastTrace(trace) }
        )
      })
    }
  }

  process () {
    this.Master = Loader.get('Master')

    this.Master.registerCommand({
      command: 'dashboardStructureUpdate',
      level: 1,
      rules: [
        new Rule('dashboard').required().isString(),
        new Rule('structure').required().isObject()
      ],
      operator: this.dashboardStructureUpdate.bind(this)
    })
    this.Master.registerCommand({
      command: 'dashboardDevicePush',
      level: 1,
      rules: [
        new Rule('dashboard').required().isString(),
        new Rule('device').required().isString(),
        new Rule('port').required().isString(),
        new Rule('push').required()
      ],
      operator: this.dashboardDevicePush.bind(this)
    })
    this.Master.registerCommand({
      command: 'dashboardDeviceAction',
      level: 2,
      rules: [
        new Rule('dashboard').required().isString(),
        new Rule('device').required().isString(),
        new Rule('action').required().isString(),
        new Rule('push').required()
      ],
      operator: this.dashboardDeviceAction.bind(this)
    })
    this.Master.registerCommand({
      command: 'dashboardDeviceOutputListen',
      level: 2,
      rules: [
        new Rule('dashboard').required().isString(),
        new Rule('device').required().isString(),
        new Rule('port').required().isString(),
        new Rule('timeout').required().default(5000).isInteger()
      ],
      operator: this.dashboardDeviceOutputListen.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboardShares',
      level: 3,
      rules: [
        new Rule('dashboard').required().isString()
      ],
      operator: this.dashboardShares.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboardSystemEvent',
      level: 3,
      rules: [
        new Rule('dashboard').required().isString()
      ],
      operator: this.dashboardSystemEvent.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboardStructure',
      level: 3,
      rules: [
        new Rule('dashboard').required().isString()
      ],
      operator: this.dashboardStructure.bind(this)
    })
  }

  async dashboardSystemEvent (event) {
    return this._dashboard.SystemEvents.emit(event.data.event, event.data.data)
  }

  async dashboardStructureUpdate (data) {
    return this._dashboard.updateStructure(data.data.structure)
  }

  async dashboardDevicePush (data) {
    if (!this._dashboard.devices[data.data.device]) throw new CommandError('Device not found')
    const device = this._dashboard.devices[data.data.device]
    if (!device.inputs[data.data.port]) throw new CommandError('Port not found')
    const inputAction = VRackUtility.camelize('input.' + data.data.port)
    return await device[inputAction](data.data.push)
  }

  async dashboardDeviceAction (data) {
    if (!this._dashboard.devices[data.data.device]) throw new CommandError('Device not found')
    const device = this._dashboard.devices[data.data.device]
    const deviceActions = this._dashboard.actions[device.id]
    const action = VRackUtility.toPath(data.data.action)
    if (!deviceActions[action]) throw new CommandError('Action on device not found')
    if (Array.isArray(deviceActions[action])) {
      if (!Validator.validate(deviceActions[action], data.data.push)) throw new DeviceError(`Push validation errors: ${Validator.toString()}`)
    }
    var funcName = VRackUtility.camelize('action.' + action)
    var ab = {
      push: data.data.push,
      clientID: data.clientID,
      index: data.__index
    }
    try {
      ab.resultData = await device[funcName](data.data.push)
      ab.result = 'success'

      this.Provider.broadcast(
        this.#makeBroadcastChannel(data.data.device, 'action'),
        { message: data.data.action, trace: this.#makeBroadcastTrace(ab) }
      )
    } catch (error) {
      ab.result = 'error'
      ab.resultData = error.toString()
      this.Provider.broadcast(
        this.#makeBroadcastChannel(data.data.device, 'action'),
        { message: data.data.action, trace: this.#makeBroadcastTrace(ab) }
      )
      throw error
    }
    return ab.resultData
  }

  dashboardDeviceOutputListen (data) {
    return new Promise((resolve, reject) => {
      if (!this._dashboard.devices[data.data.device]) reject(new CommandError('Device not found'))
      const device = this._dashboard.devices[data.data.device]
      if (!device.outputs[data.data.port]) reject(new CommandError('Port not found'))
      device.outputs[data.data.port].listens.set(data.__index, (result) => {
        clearTimeout(this._ListenPull.get(data.__index))
        this._ListenPull.delete(data.__index)
        resolve(result)
      })
      this._ListenPull.set(data.__index, setTimeout(() => {
        reject(new ListenError('Listen timeout'))
      }, data.data.timeout))
    })
  }

  async dashboardShares (data) {
    this._renderPull = this._renderAccess
    if (!this._renderTimer) this._render()
  }

  async dashboardStructure (data) {
    return this._dashboard.structure
  }

  /**
   * Создает путь до канала устройства
   * вида `dashboards.dash-id.DeviceId.render`
   * и вовзращает его
   *
   * @param {String} device Идентификатор устройства
   * @param {String} target Таргет устройства
  */
  #makeBroadcastChannel = function (device, target) {
    return ['dashboards', this._dashboard.id, device, target].join('.')
  }

  /**
   * Преобразует временную trace в строку
   */
  #makeBroadcastTrace = function (trace) {
    let strTrace = ''
    if (typeof trace === 'string') { strTrace = trace } else {
      strTrace = util.inspect(trace, {
        showHidden: false,
        depth: null,
        compact: false
      })
    }
    return strTrace
  }

  _render () {
    if (!this._renderPull.size) return
    const deviceList = Array.from(this._renderPull.values())
    for (const device of deviceList) {
      this.Provider.broadcast(
        this.#makeBroadcastChannel(device, 'render'),
        this._dashboard.devicesShares[device]
      )
    }
    this._renderPull = new Set()
    this._renderTimer = setTimeout(() => {
      this._renderTimer = false
      this._render()
    }, this.minRenderDelay)
  }
}
