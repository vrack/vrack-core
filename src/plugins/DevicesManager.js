const fs = require('fs')
const path = require('path')
const VRackUtility = require('../helpers/Utility')
const Loader = require('vrack-loader')
const BasicPlagin = require('./BasicPlugin')
const ValidatorClass = require('../helpers/validator/Validator')
const Validator = new ValidatorClass()
const DeviceError = require('../errors/DeviceError')

module.exports = class extends BasicPlagin {
  init () {
    this._devices = new Map()
    this._deviceDirectorys = []
    if (this.config.devicesDirs) for (const dir of this.config.devicesDirs) this._deviceDirectorys.push(dir)
    for (const key in this._deviceDirectorys) this._scanDir(this._deviceDirectorys[key])
    this.Master = Loader.get('Master')
  }

  process () {
    this.Master.registerCommand({
      command: 'deviceListUpdate',
      level: 3,
      rules: [],
      operator: this.deviceListUpdate.bind(this)
    })
  }

  async deviceListUpdate () {
    var result = []
    for (const dir of this._deviceDirectorys) {
      const dList = this.#devicesList(dir)
      for (const dgroup of dList) {
        const gReadmePath = path.join(Loader.systemPath, dir, dgroup.name, 'readme.md')
        if (fs.existsSync(gReadmePath)) dgroup.description = fs.readFileSync(gReadmePath).toString('utf8')
        dgroup.devices = []
        for (const device of dgroup.deviceList) {
          const ndevice = { name: device, errors: [] }
          dgroup.devices.push(ndevice)
          const devicePath = path.join(Loader.systemPath, dir, dgroup.name, device + '.js')
          if (!fs.existsSync(devicePath)) { ndevice.errors.push('File of device not found'); continue }
          try {
            const ddPath = path.join(Loader.systemPath, dir, dgroup.name, device + '.md')
            const DObject = new (require(devicePath))()
            const rules = DObject.checkParams()
            if (Array.isArray(rules) && rules.length) {
              if (!Validator.validate(rules, DObject.params, true)) {
                throw new DeviceError(`Parameter validation errors: ${Validator.toString()}`)
              }
            }
            ndevice.params = this.#exportDeviceParams(DObject)
            ndevice.ports = this.#exportDevicePorts(DObject)
            ndevice.actions = this.#exportDeviceActions(DObject)
            ndevice.description = DObject.description
            if (fs.existsSync(ddPath)) ndevice.description = fs.readFileSync(ddPath).toString('utf8')
          } catch (error) { ndevice.errors.push(error.toString()); continue }
        }
      }
      result = result.concat(dList)
    }
    return result
  }

  #exportDeviceActions = function (DObject) {
    const deviceActions = DObject.actions()
    const actions = []
    for (const action of deviceActions) {
      const exAction = action.export()
      const rExp = []
      for (const rule of exAction.rules) rExp.push(rule.export(true))
      actions.push({
        name: exAction.name,
        description: exAction.description,
        return: exAction.return,
        rules: rExp
      })
    }
    return actions
  }

  #exportDevicePorts = function (DObject) {
    const ports = DObject.ports()
    const result = []
    for (const port of ports) result.push(port.export())
    return result
  }

  #exportDeviceParams = function (DObject) {
    const params = DObject.checkParams()
    const result = []
    for (const param of params) result.push(param.export(true))
    return result
  }

  #devicesList = function (dir) {
    const result = []
    const devicesDir = path.join(Loader.systemPath, dir)
    const files = fs.readdirSync(devicesDir)
    const dirs = []
    for (var i in files) { if (fs.statSync(path.join(devicesDir, files[i])).isDirectory()) dirs.push(files[i]) }
    dirs.sort()
    for (const ddir of dirs) {
      const group = {
        name: ddir,
        dir: dir,
        devices: [],
        errors: [],
        description: ''
      }
      result.push(group)
      const listPath = path.join(devicesDir, ddir, 'list.json')
      if (!fs.existsSync(listPath)) {
        group.errors.push(listPath + ' ignored - list.json not found'); continue
      }
      const list = VRackUtility.validJSON(fs.readFileSync(listPath))
      if (!list || !Array.isArray(list)) {
        group.errors.push(listPath + ' ignored - list.json incorrect'); continue
      }
      list.sort()
      group.deviceList = list
    }
    return result
  }

  get (name) {
    if (this._devices.has(name)) return require(this._devices.get(name))
  }

  _scanDir (dir) {
    const devicesDir = path.join(Loader.systemPath, dir)
    const files = fs.readdirSync(devicesDir)
    const dirs = []
    for (var i in files) { if (fs.statSync(path.join(devicesDir, files[i])).isDirectory()) dirs.push(files[i]) }
    dirs.sort()
    for (const key in dirs) {
      const listPath = path.join(devicesDir, dirs[key], 'list.json')
      if (fs.existsSync(listPath)) {
        const list = VRackUtility.validJSON(fs.readFileSync(listPath))
        if (list) {
          for (const listKey in list) {
            const reqPath = path.join(devicesDir, dirs[key], list[listKey])
            const devicePath = reqPath + '.js'
            if (fs.existsSync(devicePath)) {
              if (!this._devices.has(dirs[key] + '.' + list[listKey])) this._devices.set(dirs[key] + '.' + list[listKey], reqPath)
              else console.log(devicePath + ' ignored - device not found')
            }
          }
        } else console.log(listPath + ' ignored - list.json incorrect')
      } else console.log(dir + dirs[key] + ' ignored - list.json not found')
    }
  }
}
