const fs = require('fs')
const path = require('path')
const Loader = require('vrack-loader')

module.exports = class {
  Queue = new Map()
  Converters = new Map()

  registerConverter (ext, cb) {
    if (ext === 'json') throw new Error('Converter extension cannot be json ')
    this.Converters.set(ext, cb)
  }

  process () {
    const DashboardsManager = Loader.get('DashboardsManager')
    for (const dir of DashboardsManager.config.dashboardDirs) this.watch(path.join(Loader.systemPath, dir))
  }

  watch (dir) {
    fs.watch(path.join(dir), { encoding: 'utf8' }, (eventType, filename) => {
      if (!filename) return
      switch (eventType) {
        case 'change':
          var res = filename.split('.')
          if (res.length !== 2) return
          if (!this.Converters.has(res[1])) return
          if (this.Queue.has(filename)) clearTimeout(this.Queue.get(filename))
          this.Queue.set(filename, setTimeout(() => {
            this.convert(dir, res[0], res[1], filename)
          }, 500))
          break
      }
    })
  }

  convert (dir, dashboard, ext, filename) {
    var conv = this.Converters.get(ext)
    var result = conv(dir, filename)
    if (result && typeof result === 'object') {
      fs.writeFileSync(path.join(dir, dashboard + '.json'), JSON.stringify(result, null, '\t'))
    }
  }
}
