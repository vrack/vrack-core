module.exports = class {
  #settings = {
    name: '',
    direct: 'input',
    data: [],
    description: '',
    mode: 1,
    required: false,
    dynamic: false,
    count: 0
  }

  #list = [
    'name',
    'direct',
    'data',
    'description',
    'mode',
    'required',
    'dynamic',
    'count'
  ]

  constructor (name) {
    this.#settings.name = name
  }

  input () {
    this.#settings.direct = 'input'
    return this
  }

  output () {
    this.#settings.direct = 'output'
    return this
  }

  data (data) {
    this.#settings.data.push(data)
    return this
  }

  description (descr) {
    this.#settings.description = descr
    return this
  }

  dynamic (count) {
    this.#settings.dynamic = true
    this.#settings.count = count
    return this
  }

  required () {
    this.#settings.required = true
    return this
  }

  modeStandart () {
    this.#settings.mode = 1
    return this
  }

  modeReturn () {
    this.#settings.mode = 2
    return this
  }

  export () {
    const res = {}
    for (const prop of this.#list) res[prop] = this.#settings[prop]
    return res
  }
}
