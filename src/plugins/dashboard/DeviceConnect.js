const DashboardError = require('../../errors/DashboardError')

module.exports = class DeviceConnect {
  init (output, input) {
    this.outputLink = output
    this.outputLink.connected = true
    this.inputLink = input
    this.inputLink.connected = true
    if (output.port.mode !== input.port.mode) throw new DashboardError(`Incompatible port mode between ${output.id} & ${input.id}`)
    this.outputLink.addConnection(this)
  }

  push (data) {
    return this.inputLink.push(data, this.outputLink)
  }
}
