const util = require('util')
module.exports = class {
  connected = false
  connections = []
  listens = new Map()

  constructor (port) {
    this.port = port
    this.id = this.port.name
  }

  addListen (index, cb) {
    this.listens[index] = cb
  }

  removeListen (index) {
    this.listens.splice(index, 1)
  }

  addConnection (connection) {
    this.connections.push(connection)
  }

  push (data) {
    if (this.listens.size) {
      var res = util.format(data)
      for (const ls of this.listens.values()) ls(res)
      this.listens = new Map()
    }
    if (this.connected) {
      if (this.connections.length === 1) return this.connections[0].push(data)
      for (var connect of this.connections) connect.push(data)
    }
  }
}
