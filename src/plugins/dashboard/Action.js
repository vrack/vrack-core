module.exports = class {
  #settings = {
    name: '',
    rules: [],
    description: '',
    return: ''
  }

  #list = [
    'name',
    'rules',
    'description',
    'return'
  ]

  constructor (name) {
    this.#settings.name = name
  }

  rules (rules) {
    this.#settings.rules = rules
    return this
  }

  return (ret) {
    this.#settings.return = ret
    return this
  }

  description (descr) {
    this.#settings.description = descr
    return this
  }

  export () {
    const res = {}
    for (const prop of this.#list) res[prop] = this.#settings[prop]
    return res
  }
}
