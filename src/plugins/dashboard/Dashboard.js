
const Loader = require('vrack-loader')
const EventEmitter = require('events')

const VRackUtility = require('../../helpers/Utility')
const DevicePort = require('./DevicePort')
const DeviceConnect = require('./DeviceConnect')
const ValidatorClass = require('../../helpers/validator/Validator')
const Validator = new ValidatorClass()
const Rule = require('../../helpers/validator/Rule')

const {
  workerData
} = require('worker_threads')

const fs = require('fs')
const DashboardError = require('../../errors/DashboardError')
const DeviceError = require('../../errors/DeviceError')

module.exports = class Dashboard extends EventEmitter {
  structure = {}
  meta = {}
  settingsRules = [
    new Rule('message').required().default(true).isBoolean(),
    new Rule('shares').required().default(true).isBoolean()
  ]

  actions = {}

  init () {
    this.id = workerData.dashboard.id
    this.meta = workerData.meta
    this.DeviceManager = Loader.get('DevicesManager')
    this.DashboardStorage = Loader.get('DashboardStorage')
    this.WorkerProvider = Loader.get('WorkerProvider')
    this.SystemEvents = new EventEmitter()
  }

  deviceStorage (device) {
    device.storage = this.DashboardStorage.read(`${this.id}.${device.id}`)
  }

  process () {
    this.Master = Loader.get('Master')
    this.Master.registerCommand({
      command: 'dashboardRun',
      level: 1,
      rules: [],
      operator: this.run.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboardCheck',
      level: 1,
      rules: [],
      operator: this.check.bind(this)
    })
  }

  check () {
    try {
      var config = JSON.parse(fs.readFileSync(workerData.dashboard.path))
      this.dashboardConfig = config
      this.initExdendsConfig()
      this.devices = {}
      this.connections = {}
      this.devicesParams = {}
      this.devicesShares = {}

      for (var key in config.devices) {
        this.devices[config.devices[key].id] = this._initDevice(config.devices[key])
        this.devicesParams[config.devices[key].id] = this.devices[config.devices[key].id].params
      }
      for (key in config.connections) this.connections[key] = this._initConnection(config.connections[key])
    } catch (error) {
      this.terminate(error, 'init')
    }
    return 'success'
  }

  run (command) {
    this.check()
    for (const key in this.devices) {
      try {
        this.devices[key].process()
      } catch (err) {
        this.devices[key].terminate(new DeviceError(err.message), 'process')
      }
    }

    try {
      for (const key in this.devices) this.devicesShares[key] = this.devices[key].shares
      var structure = {}
      if (fs.existsSync(workerData.dashboard.structurePath)) {
        structure = JSON.parse(fs.readFileSync(workerData.dashboard.structurePath))
      }
      this.updateStructure(structure)
    } catch (error) {
      this.terminate(error)
    }
  }

  updateStructure (structure) {
    for (const key in this.devices) if (structure[key] && structure[key].display) this.structure[key].display = structure[key].display
    fs.writeFileSync(workerData.dashboard.structurePath, JSON.stringify(this.structure))
  }

  terminate (error, action) {
    if (typeof error === 'string') error = new DashboardError(error)
    if (typeof error !== 'object') error = (new DashboardError()).import(error)
    if (error.name !== 'DeviceError' && error.name !== 'DashboardError') {
      const nerror = new DashboardError()
      nerror.import(error)
      nerror.stack = error.stack
      error = nerror
    }
    if (action === undefined || typeof action !== 'string') action = 'unknow'
    error.action = action
    this.WorkerProvider.terminate(error)
  }

  initExdendsConfig () {
    this.extendsConfig = { devices: [] }
    if (fs.existsSync(workerData.dashboard.configPath)) this.extendsConfig = JSON.parse(fs.readFileSync(workerData.dashboard.configPath))
    if (this.extendsConfig.devices === undefined || !Array.isArray(this.extendsConfig.devices)) this.extendsConfig.devices = []
    for (const device of this.extendsConfig.devices) {
      if (!device.id || device.params === undefined || typeof device.params !== 'object') continue
      for (const cdev of this.dashboardConfig.devices) {
        if (cdev.id === device.id) {
          for (const pname in device.params) cdev.params[pname] = device.params[pname]
        }
      }
    }
  }

  _initDevice (deviceConfig) {
    var Сl = this.DeviceManager.get(deviceConfig.type)
    // Проверяем наличие валидного ID
    if (deviceConfig.id === undefined || !deviceConfig.id) throw new DashboardError('Device ID not found - ' + JSON.stringify(deviceConfig))
    // Наличие дубликатов
    if (deviceConfig.id in this.devices) throw new DashboardError('Device ID ' + deviceConfig.id + ' dublicate')
    // Наличие девайса в списке поддерживаемых девайсов
    if (Сl === undefined) throw new DashboardError("Device type '" + deviceConfig.type + "' not found ")
    var deviceSettings = {}
    var newDevice = new Сl()
    newDevice.id = deviceConfig.id
    newDevice.type = deviceConfig.type
    newDevice.dashboard = this
    // Инициализация хранилища
    this.deviceStorage(newDevice)
    // Установка парамметров
    for (var key in deviceConfig.params) newDevice.params[key] = deviceConfig.params[key]

    // Валидация параметров
    const rules = newDevice.checkParams()
    if (Array.isArray(rules) && rules.length) {
      if (!Validator.validate(rules, newDevice.params)) {
        const nError = new DeviceError(`Parameter validation errors: ${Validator.toString()}`)
        newDevice.terminate(nError, 'init')
      }
    }

    // запуск препроцесса
    try {
      newDevice.preProcess()
      deviceSettings = newDevice.settings()
    } catch (err) {
      newDevice.terminate(err, 'preprocess')
    }

    // Переопределние настроек устройства
    if (deviceConfig.settings) {
      for (var k in deviceConfig.settings) deviceSettings[k] = deviceConfig.settings[k]
    }

    if (!Validator.validate(this.settingsRules, deviceSettings)) {
      const nError = new DeviceError(`Settings validation errors: ${Validator.toString()}`)
      newDevice.terminate(nError, 'init')
    }

    const deviceActions = newDevice.actions()
    const structureActions = {}
    for (const action of deviceActions) {
      const exAction = action.export()
      this._addDeviceAction(exAction.name, exAction.rules, newDevice)
      const rExp = []
      for (const rule of exAction.rules) rExp.push(rule.export(true))
      structureActions[exAction.name] = {
        name: exAction.name,
        description: exAction.description,
        return: exAction.return,
        rules: rExp
      }
    }

    this.structure[newDevice.id] = {
      id: deviceConfig.id,
      type: deviceConfig.type,
      outputs: {},
      inputs: {},
      ports: [],
      settings: deviceSettings,
      actions: structureActions
    }

    // Создание динамических портов
    const ports = []
    for (const port of newDevice.ports()) {
      const exPort = port.export()
      if (!exPort.dynamic) {
        ports.push(exPort)
        continue
      }
      if (!exPort.name.match(/%d/)) {
        const nError = new DeviceError(`Incorrect name of device dynamic port ${exPort.name} use 'port%d' template`)
        newDevice.terminate(nError, 'init')
      }
      for (var i = 1; i <= exPort.count; i++) {
        const cpPort = Object.assign({}, exPort)
        cpPort.dynamic = false
        cpPort.count = 0
        cpPort.name = cpPort.name.replace(/%d/, i)
        ports.push(cpPort)
      }
    }

    // Добавление портов к устройству
    for (const port of ports) {
      const dport = new DevicePort(port)
      dport.device = newDevice
      this.structure[newDevice.id].ports.push(dport.port)
      const subItems = dport.id.split('.')
      if (dport.port.direct === 'output') {
        newDevice.outputs[dport.id] = dport
        if (subItems.length === 2) {
          if (!newDevice.outputsGroups[subItems[0]]) newDevice.outputsGroups[subItems[0]] = []
          newDevice.outputsGroups[subItems[0]].push(dport)
        }
        this.structure[newDevice.id].outputs[dport.id] = null
      } else {
        newDevice.inputs[dport.id] = dport
        if (!newDevice[VRackUtility.camelize('input.' + dport.id)]) throw new DashboardError('Input handler not found ' + JSON.stringify(deviceConfig))
        dport.push = newDevice[VRackUtility.camelize('input.' + dport.id)].bind(newDevice)
        if (subItems.length === 2) {
          if (!newDevice.inputsGroups[subItems[0]]) newDevice.inputsGroups[subItems[0]] = []
          newDevice.inputsGroups[subItems[0]].push(dport)
        }
        this.structure[newDevice.id].inputs[dport.id] = null
      }
    }

    if (deviceSettings.shares) {
      newDevice.render = () => {
        this.emit('broadcastRender', newDevice.id)
      }
    }

    newDevice.save = () => {
      this.DashboardStorage.write(`${this.id}.${newDevice.id}`, newDevice.storage)
    }
    newDevice.error = (data, trace = {}) => {
      this.emit('broadcastError', newDevice.id, data, trace)
    }
    newDevice.notify = (data, trace = {}) => {
      this.emit('broadcastNotify', newDevice.id, data, trace)
    }
    newDevice.alert = (data, trace = {}) => {
      this.emit('broadcastAlert', newDevice.id, data, trace)
    }
    newDevice.terminal = (data, trace = {}) => {
      this.emit('broadcastTerminal', newDevice.id, data, trace)
    }
    newDevice.event = (event, data = {}) => {
      this.emit('broadcastEvent', newDevice.id, event, data)
    }
    return newDevice
  }

  _addDeviceAction (action, rules, device) {
    if (!this.actions[device.id]) this.actions[device.id] = {}
    this.actions[device.id][action] = rules
  }

  _initConnection (ccStr) {
    var cc = this._toConnection(ccStr)
    var con = new DeviceConnect()
    // Проверка исходящего устройства
    if (!(cc.outputDevice in this.devices)) throw new DashboardError("Output device not found - '" + cc.outputDevice + "'")
    if (!(cc.outputPort in this.devices[cc.outputDevice].outputs)) throw new DashboardError("Output device port connection not found - '" + ccStr + "'")

    // Проверка входящего устройства
    if (!(cc.inputDevice in this.devices)) throw new DashboardError("Input device connection not found - '" + ccStr + "'")
    if (!(cc.inputPort in this.devices[cc.inputDevice].inputs)) throw new DashboardError("Input device port connection not found - '" + ccStr + "'")

    try {
      con.init(this.devices[cc.outputDevice].outputs[cc.outputPort], this.devices[cc.inputDevice].inputs[cc.inputPort])
      if (this.structure[cc.outputDevice].outputs[cc.outputPort] === null) this.structure[cc.outputDevice].outputs[cc.outputPort] = []
      this.structure[cc.inputDevice].inputs[cc.inputPort] = { device: cc.outputDevice, port: cc.outputPort }
      this.structure[cc.outputDevice].outputs[cc.outputPort].push({ device: cc.inputDevice, port: cc.inputPort })
    } catch (err) {
      this.devices[cc.outputDevice].terminate(err, 'init')
    }
    return con
  }

  _toConnection (conStr) {
    var act = conStr.split('->')
    if (act.length !== 2) throw new DashboardError('Syntax connection error, syntax have -> beetwen device')
    var outputDevice = act[0].split('.')
    var inputDevice = act[1].split('.')
    if (outputDevice.length > 3 || inputDevice.length > 3) throw new DashboardError(`Syntax connection error, syntax mast have device.port or device.group.port [${act[0]}]`)
    return {
      outputDevice: outputDevice.shift().trim(),
      outputPort: outputDevice.join('.').trim(),
      inputDevice: inputDevice.shift().trim(),
      inputPort: inputDevice.join('.').trim()
    }
  }
}
