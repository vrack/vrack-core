const DeviceError = require('../../errors/DeviceError')
module.exports = class Device {
  id = '';
  description = '';
  outputsGroups = {};
  outputs = {};
  settings () {
    return {
      message: true,
      shares: true
    }
  }

  inputsGroups = {};
  inputs = {};
  params = {};
  shares = {};
  storage = {};
  actions () { return [] };
  checkParams () { return [] };
  ports () { return [] }
  preProcess () {};
  process () {};
  render () {};
  save () {};
  error (data, trace) {};
  notify (data, trace) {};
  alert (data, trace) {};
  terminal (data, trace) {};
  event (data, trace) {};
  terminate (error, action) {
    if (typeof error === 'string') error = new DeviceError(error)
    if (typeof error !== 'object') error = new DeviceError('Terminate error type is undefined')
    if (error.name && error.name !== 'DeviceError') {
      const nerror = new DeviceError(error.message)
      nerror.stack = error.stack
      error = nerror
    }
    error.deviceId = this.id
    this.dashboard.terminate(error, action)
  }
}
