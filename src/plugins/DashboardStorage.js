const fs = require('fs')
const path = require('path')

const {
  workerData
} = require('worker_threads')

module.exports = class {
  init () {
    if (this.config.storagePath === undefined) {
      this.storagePath = './storage'
    } else {
      this.storagePath = this.config.storagePath
    }

    if (this.config.revisions === undefined) {
      this.revisions = 32
    } else {
      this.revisions = this.config.revisions
    }

    const storageDashboardPath = path.join(this.storagePath, workerData.dashboard.id)
    if (!fs.existsSync(storageDashboardPath)) fs.mkdirSync(storageDashboardPath)
    if (!fs.existsSync(this.storagePath)) fs.mkdirSync(this.storagePath)
  }

  read (spath) {
    var sdir = path.join(this.storagePath, spath.split('.').join('/'))
    if (!fs.existsSync(sdir) || !fs.lstatSync(sdir).isDirectory()) return {}
    return this.searchRevision(sdir)
  }

  write (spath, data) {
    var sdir = path.join(this.storagePath, spath.split('.').join('/'))
    if (!fs.existsSync(sdir)) this.createOwner(spath)
    this.addRevision(sdir, data)
  }

  createOwner (spath) {
    var acts = spath.split('.')
    var npath = path.join(this.storagePath)
    for (var act of acts) {
      npath = path.join(npath, act)
      if (!fs.existsSync(npath)) fs.mkdirSync(npath)
    }
  }

  searchRevision (sdir) {
    var result = fs.readdirSync(sdir)
    if (!result.length) return {}
    result.sort((a, b) => {
      if (a < b) return 1
      if (a > b) return -1
      return 0
    })
    for (const d of result){
      var content = fs.readFileSync(path.join(sdir, d))
      try {
        const obj = JSON.parse(content)
        return obj
      }catch (e){
        console.log('Error read revision (skeep): ', e.toString())
      }
    }
    return {}
  }

  addRevision (sdir, data) {
    var result = fs.readdirSync(sdir)
    if (result.length === 0) {
      fs.writeFileSync(path.join(sdir, Date.now().toString()), JSON.stringify(data))
      return
    }
    result.sort()
    var filename = Date.now().toString()
    fs.writeFileSync(path.join(sdir, filename), JSON.stringify(data))
    if (result.length >= this.revisions) fs.unlinkSync(path.join(sdir, result[0]))
  }
}
