const {
  parentPort
} = require('worker_threads')

module.exports = class {
  constructor (config, SL) {
    if (!config) config = {}
    if (!config.delay) config.delay = 5000
    this.config = config
    this.SL = SL
  }

  init () {}

  process () {
    setInterval(this._sendMemoryUsage.bind(this), this.config.delay)
  }

  _sendMemoryUsage () {
    parentPort.postMessage({
      command: 'memoryUsage',
      dashboard: this.SL.get('Dashboard').id,
      data: process.memoryUsage()
    })
  }
}
