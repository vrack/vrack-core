// const fs = require('fs')
const Loader = require('vrack-loader')
const BasicPlagin = require('./BasicPlugin')
const Rule = require('../helpers/validator/Rule')
const CommandError = require('../errors/CommandError')

module.exports = class extends BasicPlagin {
  checkParams () {
    return [
      new Rule('devicesConfig').required().default('./config.devices.json').isFile()
    ]
  }

  init () {
    this.Master = Loader.get('Master')
    this.WorkersManager = Loader.get('WorkersManager')
    this.rawList = []
    this.vendorList = []
  }

  process () {
    this.Master.registerCommand({
      command: 'deviceListUpdate',
      level: 3,
      rules: [],
      operator: this.deviceListUpdate.bind(this),
      description: 'Обновление списка устройств'
    })

    this.Master.registerCommand({
      command: 'deviceVendorList',
      level: 3,
      rules: [],
      operator: this.deviceVendorList.bind(this),
      description: 'Возвращает список вендоров'
    })

    this.Master.registerCommand({
      command: 'deviceVendorInformation',
      level: 3,
      rules: [
        new Rule('vendor').required().isString()
      ],
      operator: this.deviceVendorInformation.bind(this),
      description: 'Возвращает информацию о вендоре и список устройств'
    })

    this.Master.registerCommand({
      command: 'deviceInformation',
      level: 3,
      rules: [
        new Rule('device').required().isString(),
        new Rule('vendor').required().isString()
      ],
      operator: this.deviceInformation.bind(this),
      description: 'Возвращает информацию об устройстве'
    })

    this.deviceListUpdate({
      command: 'deviceListUpdate',
      client: { level: 1 }
    })
  }

  async deviceListUpdate (command, client) {
    const wid = await this.WorkersManager.addWorker({
      fileConfig: this.config.devicesConfig
    }, (error) => {
      this.WorkersManager.stopWorker(wid)
      throw error
    })
    try {
      this.rawList = await this.WorkersManager.request(wid, command)
      this.vendorList = []
      for (const group of this.rawList) {
        this.vendorList.push({
          name: group.name,
          dir: group.dir,
          errors: group.errors
        })
      }
      await this.WorkersManager.stopWorker(wid)
      return this.vendorList
    } catch (error) {
      await this.WorkersManager.stopWorker(wid)
      throw error
    }
  }

  async deviceVendorList () {
    return this.vendorList
  }

  async deviceVendorInformation (command, client) {
    const Vendor = this.searchVendor(command.data.vendor)

    const dlist = []
    for (const device of Vendor.devices) {
      dlist.push({ errors: device.errors, name: device.name })
    }
    return {
      name: Vendor.name,
      dir: Vendor.dir,
      errors: Vendor.errors,
      description: Vendor.description,
      devices: dlist
    }
  }

  /**
   * Возвращает информацию по устройству
   * */
  async deviceInformation (command, client) {
    return this.searchDevice(command.data.vendor, command.data.device)
  }

  /**
   * Поиск вендора по имени
   *
   * @param {String} vendor
  */
  searchVendor = function (vendor) {
    for (const data of this.rawList) if (data.name === vendor) return data
    throw new CommandError(`Vendor ${vendor} not found`)
  }

  /**
   * Поиск вендора по имени
   *
   * @param {String} vendor
   * @param {String} device
  */
  searchDevice = function (vendor, device) {
    const Vendor = this.searchVendor(vendor)
    for (const data of Vendor.devices) if (data.name === device) return data
    throw new CommandError(`Device ${vendor} not found`)
  }
}
