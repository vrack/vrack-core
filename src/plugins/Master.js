
const Loader = require('vrack-loader')

const ValidatorClass = require('../helpers/validator/Validator')
const Validator = new ValidatorClass()
const CommandError = require('../errors/CommandError')
const ValidationError = require('../errors/ValidationError')

module.exports = class {
  commands = {}
  subMaster = null
  clients = new Map()

  process () {
    this.DashboardsManager = Loader.get('DashboardsManager')
  }

  /**
   * @param  {array} commands
   */
  registerCommands (commands) {
    for (const command of commands) this.registerCommand(command)
  }

  /**
   * Submaster register callback
   *
   * @param {CallableFunction} cb - Callback function
  */
  registerSubMaster (cb) {
    this.subMaster = cb
  }

  /**
   * Command format
   *
   * ```js
   * {
   *  command: 'dashboardListUpdate',
   *  level: 1,
   *  operator: function (){...}
   * }
   * ```
  */
  registerCommand (command) {
    this.commands[command.command] = command
  }

  getCommand (command) {
    if (!this.commands[command]) return false
    return this.commands[command]
  }

  async command (command, client) {
    var inCommand = this.getCommand(command.command)
    if (!inCommand) {
      if (this.subMaster) return this.subMaster(command, client)
      else throw new CommandError('Command not registered')
    }
    if (client.level > inCommand.level) throw new CommandError('Access is denied for this command')
    if (Array.isArray(inCommand.rules) && inCommand.rules.length) {
      if (!Validator.validate(inCommand.rules, command.data)) throw new ValidationError(`Command validation errors: ${Validator.toString()}`)
    }
    return await inCommand.operator(command, client)
  }
}
