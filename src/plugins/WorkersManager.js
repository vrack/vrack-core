const { Worker } = require('worker_threads')
const WorkerError = require('../errors/WorkerError')
const Loader = require('vrack-loader')
const VRackError = require('../errors/Error')
module.exports = class {
  Queue = new Map()
  #workers = new Map()
  #workersQueue = []
  #workerIndex = 0;
  #messageIndex = 0;

  init () {
    this.Guard = Loader.get('Guard')
    this.Master = Loader.get('Master')
  }

  /**
   * Добавления воркера
  */
  addWorker (data, onError, onExit) {
    return new Promise((resolve, reject) => {
      const id = this.#getWorkerIndex()
      const index = this.#addWorkerQueue(id, resolve, reject)
      this.#workersQueue[id] = []
      data.__index = index
      data.__id = id
      const nWorker = new Worker('./index.js', { workerData: data })
      this.#workers.set(id, nWorker)
      nWorker.on('message', (data) => {
        if (data.internal) return this.internal(id, data)
        if (data.command === 'broadcast') return this.Guard.broadcast(data)
        if (data.command === 'error') return nWorker.emit('error', (new VRackError()).import(data.error))
        if (data.__index && this.Queue.has(data.__index)) {
          var prom = this.Queue.get(data.__index)
          this.Queue.delete(data.__index)
          var index = this.#workersQueue[id].indexOf(data.__index)
          if (index !== -1) this.#workersQueue[id].splice(index, 1)
          if (data.result === 'error') {
            prom.reject((new VRackError()).import(data.resultData))
          }
          prom.resolve(data.resultData)
        } else {
          if (data.command === 'error' &&
          onError && typeof onError === 'function') return onError(data.error)
        }
      })

      nWorker.on('exit', (data) => {
        for (var queue of this.#workersQueue[id]) {
          const prom = this.Queue.get(queue)
          this.Queue.delete(queue)
          prom.reject(new WorkerError('Dashboard exit'))
        }
        this.#workers.delete(id)
        this.#workersQueue[id] = null
      })

      if (onError && typeof onError === 'function') nWorker.on('error', onError)
      if (onExit && typeof onExit === 'function') nWorker.on('exit', onExit)
    })
  }

  /**
   * Выполнение команды из дашборда
   *
  */
  async internal (id, command) {
    const notInternalCommand = {
      command: command.command,
      data: command.data
    }
    const client = { level: 0, id: 0 }
    var result = 'success'
    var resultData = ''
    try {
      resultData = await this.Master.command(notInternalCommand, client)
    } catch (error) {
      resultData = VRackError.objectify(error)
      result = 'error'
    }
    command.result = result
    command.resultData = resultData
    this.#workers.get(id).postMessage(command)
  }

  /**
   * Останавливает работу воркера
   *
   * @param {Number} id Идентификатор воркера
   * @return {Promise}
  */
  async stopWorker (id) {
    if (!this.#workers.has(id)) return true
    try {
      await new Promise((resolve, reject) => {
        this.#addWorkerQueue(id, resolve, reject)
        this.#workers.get(id).terminate()
      })
    } catch (e) {
      return true
    }
  }

  /**
   * Возвращает воркер по его идентификатору
   *
   * @param {Number} id Идентификатор воркера
   * @return {Worker}
  */
  getWorker (id) {
    return this.#workers.get(id)
  }

  /**
   * Возвращает воркер по его идентификатору
   *
   * @param {Number} id Идентификатор воркера
   * @return {Boolean}
  */
  hasWorker (id) {
    return this.#workers.has(id)
  }

  /**
   * Отправка запросов воркерам
   *
   * @param {Number} id Идентификатор воркера
   * @param {Object} data Данные для воркера
   * @return {Promise}
  */
  request (id, data) {
    return new Promise((resolve, reject) => {
      var index = this.#addWorkerQueue(id, resolve, reject)
      try {
        data.__index = index
        this.#workers.get(id).postMessage(data)
      } catch (error) {
        this.Queue.delete(index)
        reject(error)
      }
    })
  }

  /**
   * Доабвляет в очередь задачу для ответа от воркера
   *
   * Возврщает идентификатор в очереди который должен быть отправлен
   * воркеру, который должен вернуть его в ответе после выполнения запроса
   * @see this.request
   * @param {Number} id Идентификатор воркера
   * @param {Function} resolve Функция вызова при успешном выполнении
   * @param {Function} reject Функция вызова при ошибке
   * @return {Number} идентификатор задачи в очереди
  */
  #addWorkerQueue = function (id, resolve, reject) {
    var index = this.#getMessageIndex()
    this.Queue.set(index, { resolve, reject })
    if (!this.#workersQueue[id]) this.#workersQueue[id] = []
    this.#workersQueue[id].push(index)
    return index
  }

  /**
   * Возвращает новый идентификатор сообщения воркера
   *
   * @return {Number} Новый идентификатор
  */
  #getMessageIndex = function () {
    this.#messageIndex++
    return this.#messageIndex
  }

  /**
   * Возвращает новый идентификатор воркера
   *
   * @return {Number} Новый идентификатор
  */
  #getWorkerIndex = function () {
    this.#workerIndex++
    return this.#workerIndex
  }
}
