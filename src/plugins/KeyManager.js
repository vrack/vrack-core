const fs = require('fs')
const ip = require('ip')
const path = require('path')
const crypto = require('crypto')
const Loader = require('vrack-loader')
const Rule = require('../helpers/validator/Rule')

class ApiKeyManager {
  filters = new Map()

  init () {
    this.Master = Loader.get('Master')
    this.Guard = Loader.get('Guard')
    this.SystemEvents = Loader.get('SystemEvents')
  }

  process () {
    this.loadKeys()

    this.Master.registerCommand({
      command: 'apiKeyList',
      level: 1,
      operator: this.apiKeyList.bind(this)
    })

    this.Master.registerCommand({
      command: 'apiKeyAdd',
      level: 1,
      rules: [
        new Rule('level').required().isInteger().expression('value >= 1 && value <= 1000'),
        new Rule('cipher').required().default(true).isBoolean(),
        new Rule('name').required().isString(),
        new Rule('description').required().default('').isString()
      ],
      operator: this.apiKeyAdd.bind(this)
    })

    this.Master.registerCommand({
      command: 'apiKeyUpdate',
      level: 1,
      rules: [
        new Rule('key').required().isString(),
        new Rule('name').required().isString(),
        new Rule('description').required().isString()
      ],
      operator: this.apiKeyUpdate.bind(this)
    })

    this.Master.registerCommand({
      command: 'apiKeyUpdateFilter',
      level: 1,
      rules: [
        new Rule('key').required().isString(),
        new Rule('path').required().isString(),
        new Rule('list').required().isArray()
      ],
      operator: this.apiKeyUpdateFilter.bind(this)
    })

    this.Master.registerCommand({
      command: 'apiKeyRegisteredFilters',
      level: 1,
      rules: [],
      operator: this.apiKeyRegisteredFilters.bind(this)
    })

    this.Master.registerCommand({
      command: 'apiKeyDelete',
      level: 1,
      rules: [
        new Rule('key').required().isString()
      ],
      operator: this.apiKeyDelete.bind(this)
    })

    this.Master.registerCommand({
      command: 'apiKeyIpAdd',
      level: 1,
      rules: [
        new Rule('key').required().isString(),
        new Rule('network').required().isString().custom('Incorrect IP network', (data) => {
          try {
            ip.cidrSubnet(data)
          } catch {
            return false
          }
        })
      ],
      operator: this.apiKeyIpAdd.bind(this)
    })

    this.Master.registerCommand({
      command: 'apiKeyIpDelete',
      level: 1,
      rules: [
        new Rule('key').required().isString(),
        new Rule('network').required().isString().custom('Incorrect IP network', (data) => {
          try {
            ip.cidrSubnet(data)
          } catch {
            return false
          }
        })
      ],
      operator: this.apiKeyIpDelete.bind(this)
    })

    this.Master.registerCommand({
      command: 'apiKeyFilters',
      level: 3,
      rules: [],
      operator: this.apiKeyFilters.bind(this)
    })

    this.Master.registerCommand({
      command: 'apiKeyAuth',
      level: 1000,
      rules: [
        new Rule('key').required().isString()
      ],
      operator: this.apiKeyAuth.bind(this)
    })

    this.Master.registerCommand({
      command: 'apiPrivateAuth',
      level: 1000,
      rules: [
        new Rule('verify').required().isString()
      ],
      operator: this.apiPrivateAuth.bind(this)
    })
  }

  /**
   * Команда apiKeyList
   * Возвращает список всех ключей доступа
   *
   * @return {Object}
  */
  async apiKeyList () {
    return this.keys
  }

  /**
   * Команда apiKeyAdd
   * Добавление нового ключа
   *
   * @return {Object} объект нового ключа
  */
  async apiKeyAdd (command) {
    var newKey = {
      name: command.data.name,
      description: command.data.description,
      level: command.data.level,
      key: this.makeKey(8),
      filters: {}
    }
    if (command.data.cipher === true) newKey.private = this.makeKey(16)
    this.keys.push(newKey)
    this.syncKeys()
    return newKey
  }

  /**
   * Команда apiKeyUpdate
   * Обновление названия и описания ключа
   *
   * @return {String}
  */
  async apiKeyUpdate (command) {
    let updated = false
    for (var index in this.keys) {
      if (this.keys[index].key === command.data.key) {
        this.keys[index].name = command.data.name
        this.keys[index].description = command.data.description
        updated = true
      }
    }

    if (updated) {
      this.syncKeys()
      return ''
    }
    throw new Error('Key not found')
  }

  /**
   * Команда apiKeyDelete
   * Удаление ключа
   *
   * @return {String}
  */
  async apiKeyDelete (command) {
    let deleted = false
    for (var index in this.keys) {
      if (this.keys[index].key === command.data.key) {
        this.keys.splice(index, 1)
        deleted = true
      }
    }
    this.syncKeys()
    if (deleted) return ''
    throw new Error('Key not found')
  }

  /**
   * Команда apiKeyIpAdd
   * Добавление к ключу доступа IP подсеть
   *
   * @return {String}
  */
  async apiKeyIpAdd (command) {
    for (var key of this.keys) {
      if (key.key === command.data.key) {
        if (!key.ipAllow || !Array.isArray(key.ipAllow)) key.ipAllow = []
        if (key.ipAllow.indexOf(command.data.key) !== -1) return
        key.ipAllow.push(command.data.network)
        this.syncKeys()
        return ''
      }
    }
    throw new Error('Key not found')
  }

  /**
   * Команда apiKeyIpDelete
   * Удаление IP подсети
   *
   * @return {String}
  */
  async apiKeyIpDelete (command) {
    for (var key of this.keys) {
      if (key.key === command.data.key) {
        if (!key.ipAllow || !Array.isArray(key.ipAllow)) key.ipAllow = []
        if (key.ipAllow.indexOf(command.data.key) === -1) return
        key.ipAllow.splice(key.ipAllow.indexOf(command.data.network), 1)
        this.syncKeys()
        return ''
      }
    }
    throw new Error('Key not found')
  }

  /**
   * Команда apiKeyAuth
   * Первый этап авторизации
   *
   * @return {String}
  */
  async apiKeyAuth (command) {
    const client = this.Guard.getClient(command.providerID, command.clientID)
    for (var key of this.keys) {
      if (key.key === command.data.key) {
        if (!this.checkIpNetwork(client)) throw new Error('Access denied for this ip address')
        if (key.private) {
          client.cipher = false
          client.verify = this.makeKey(8)
          client.key = key
          return {
            verify: client.verify,
            cipher: true
          }
        } else {
          return this.authorize(key, client, false)
        }
      }
    }
    this.SystemEvents.emit('KeyManager.authorize.fail', client)
    throw new Error('Key not found')
  }

  /**
   * Команда apiPrivateAuth
   * Второй этап авторизации
   *
   * @return {String}
  */
  async apiPrivateAuth (command) {
    const client = this.Guard.getClient(command.providerID, command.clientID)
    try {
      const decr = this.Guard.decipherData(client, command.data.verify)
      if (decr === client.verify) return this.authorize(client.key, client, true)
    } catch (error) { console.log(error) }
    this.SystemEvents.emit('KeyManager.authorize.fail', client)
    throw new Error('Invalid private key')
  }

  /**
   * Команда apiKeyAuth
   * Первый этап авторизации
   *
   * @return {String}
  */
  async apiKeyUpdateFilter (command) {
    const acts = command.data.path.split('.')
    if (!this.filters.has(acts[0])) throw new Error('Filter not registered')
    for (const l of command.data.list) if (typeof l !== 'string') throw new Error('Incorrect filter element')
    for (var key of this.keys) {
      if (key.key === command.data.key) {
        if (!key.filters) key.filters = {}
        if (!key.filters[command.data.path]) key.filters[command.data.path] = []
        key.filters[command.data.path] = command.data.list
        this.syncKeys()
        return
      }
    }
    throw new Error('Key not found')
  }

  /**
   * Команда apiKeyRegisteredFilters
   * Получение списка зарегистрированных фильтров
   *
   * @return {String}
  */
  async apiKeyRegisteredFilters (command) {
    const result = []
    for (const filter of this.filters.values()) {
      const path = [filter.filter]
      for (const param of filter.params) path.push(`[${param}]`)
      result.push({
        name: filter.name,
        description: filter.description,
        path: path.join('.')
      })
    }
    return result
  }

  /**
   * Команда apiKeyFilters
   * Получение списка действующих для данного ключа фильтров
   *
   * @return {String}
  */
  async apiKeyFilters (command, client) {
    return client.key.filters
  }

  /**
   * Проверяет возможность подключения для этого IP
   *
   * @param {Object} key Ключ
   * @param {Object} client Клиент
  */
  checkIpNetwork (key, client) {
    if (
      key.ipAllow &&
      Array.isArray(key.ipAllow) &&
      key.ipAllow.length &&
      client.meta.ip
    ) {
      for (const ipAllow of key.ipAllow) {
        if (ip.cidrSubnet(ipAllow).contains(client.meta.ip)) return true
      }
      return false
    } else {
      return true
    }
  }

  /**
   * Регистрирует фильтр для других плагинов. После чего можно добавлять фильтры
   * со стороны клиента VRack для каждого ключа, индивидуально
   * для каждого зарегистрированного фильтра
   *
   * @param {String} name Название регистриуемого фильтра
  */
  registerFilter (path, name, description, check, cb) {
    const acts = path.split('.')
    var params = []
    if (acts.length) params = acts.splice(1, acts.length - 1)
    this.filters.set(acts[0], {
      filter: acts[0],
      name,
      description,
      params,
      check,
      cb
    })
  }

  /**
   * Применяет фильтр к входящей команде
   * Если команда не пройдет проверку фильтром  будет создано исключение
   *
   * @param {Object} command Приходящяя команда клиента VRack
  */
  filterOut (command, client) {
    if (!client.key || !client.key.filters) return
    for (const filter of this.filters.values()) {
      if (filter.check(command, client.key.filters) && filter.cb(command, client.key.filters)) {
        throw new Error(`No access to this entity: ${filter.name}`)
      }
    }
  }

  /**
   * Авторизация клиента VRack
   *
   * @param {Object} key Ключ
   * @param {Object} client Клиент VRack
   * @param {Boolean} cipher Используется ли шифрование
  */
  authorize (key, client, cipher) {
    client.key = key
    client.level = client.key.level
    client.cipher = cipher
    client.authorize = true
    this.SystemEvents.emit('KeyManager.authorize.success', client)
    return {
      level: key.level,
      cipher
    }
  }

  loadKeys () {
    this.keysPath = path.join(Loader.systemPath, this.config.keysPath)
    if (!fs.existsSync(this.keysPath)) {
      this.keys = [{
        name: 'default',
        description: 'default key',
        level: 1,
        key: 'default',
        filters: {}
      }]
    } else {
      this.keys = JSON.parse(fs.readFileSync(this.keysPath))
    }
    for (var key of this.keys) this.checkKey(key)
    this.syncKeys()
  }

  syncKeys () {
    fs.writeFileSync(this.keysPath, JSON.stringify(this.keys))
  }

  makeKey (size) {
    return crypto.randomBytes(size).toString('hex')
  }

  checkKey (key) {
    if (!key) throw new Error('Key should be object')
    if (!key.key) throw new Error('Key must contain key')
    if (!key.level) throw new Error('Key must contain level')
    if (!key.filters) key.filters = []
  }
}

module.exports = ApiKeyManager
