const Loader = require('vrack-loader')
const crypto = require('crypto')
const VRackError = require('../errors/Error')
module.exports = class {
  clients = new Map()
  providers = {}

  init () {
  }

  process () {
    this.Master = Loader.get('Master')
    this.ApiKeyManager = Loader.get('ApiKeyManager')
    this.SystemEvents = Loader.get('SystemEvents')
    this.ApiKeyManager.registerFilter(
      'commands',
      'Api commands filter',
      'Фильтрует доступ к конкретным командам API',
      function (command, filters) {
        if (filters.commands && filters.commands.length) return true
      },
      function (command, filters) {
        if (filters.commands.indexOf(command.command) === -1) return true
        return false
      }
    )
  }

  /**
   * Отправка бродкастов
   *
   * @param {Object} data Данные бродкаста
  */
  broadcast (data) {
    var chn = data.channel.split('.')
    var acts = chn.length
    for (let i = 0; i < acts; i++) {
      data.target = chn.join('.')
      Loader.get('Broadcaster').broadcast(chn.join('.'), (providerID, clientID) => {
        const Client = this.getClient(providerID, clientID)
        if (!Client) {
          Loader.get('Broadcaster').clientClear(providerID, clientID)
          return
        }
        var ret = JSON.stringify(data)
        if (Client.cipher) ret = this.cipherData(Client, ret)
        this.providers[providerID].clientSend(clientID, ret)
      })
      chn.pop()
    }
  }

  /**
   * Прослойка между провайдером и мастером
   *
   * @param {Number} providerID Идентификатор провайдера
   * @param {Number} clientID Идентификатор клиента
   * @param {String} data Raw данные запроса клиента
  */
  async receive (providerID, clientID, data) {
    const client = this.getClient(providerID, clientID)
    if (!client) throw new Error('Client not registered')
    const cipher = client.cipher

    try {
      try {
        if (client.cipher) data = this.decipherData(client, data)
      } catch (err) {
        throw new Error('Error decipher data, invalid private key?')
      }
    } catch (error) {
      return this.response(client, {}, 'error', error, false)
    }

    try {
      data = JSON.parse(data)
    } catch (error) {
      return this.response(client, {}, 'error', error, cipher)
    }

    try {
      data.providerID = providerID
      data.clientID = clientID
      this.ApiKeyManager.filterOut(data, client)
      data.client = client
      var result = await this.Master.command(data, client)
      return this.response(client, data, 'success', result, cipher)
    } catch (error) {
      if (data.command !== 'dashboardDeviceOutputListen') {
        const nError = new VRackError().import(error)
        nError.command = data.command
        nError.data = data.data
        this.SystemEvents.emit('Guard.command.error', nError.export())
      }
      return this.response(client, data, 'error', error, cipher)
    }
  }

  /**
   * Отвечает за формирование ответа для конкретного клиента
   *
   * @param {Object} client Данные клиента (см. getClient)
   * @param {Object} request Данные запроса
   * @param {String} result Результат запроса
   * @param {Object|String} resultData Данные результата запроса
   * @param {Boolean} cipher Шифрование данных
  */
  response (client, request, result, resultData, cipher) {
    request.result = result
    if (result === 'error') {
      request.resultData = VRackError.objectify(resultData)
    } else {
      request.resultData = resultData
    }
    delete request.client
    var ret = JSON.stringify(request)
    if (cipher) ret = this.cipherData(client, ret)
    return ret
  }

  /**
   * Шифрование данных клиента
   *
   * @param {Object} client Данные клиента (см. getClient)
   * @param {String} data JSON данные
  */
  cipherData (client, data) {
    const cipher = crypto.createCipheriv('aes-256-cbc', client.key.private, client.key.key)
    cipher.setEncoding('utf8')
    let encrypted = ''
    encrypted += cipher.update(data, 'utf8', 'base64')
    encrypted += cipher.final('base64')
    return encrypted
  }

  /**
   * Дешифрование данных клиента
   *
   * @param {Object} client Данные клиента (см. getClient)
   * @param {String} data Base64 Данные
  */
  decipherData (client, data) {
    const cipher = crypto.createDecipheriv('aes-256-cbc', client.key.private, client.key.key)
    cipher.setEncoding('base64')
    let decrypted = ''
    decrypted += cipher.update(data, 'base64')
    decrypted += cipher.final('utf8')
    return decrypted
  }

  /**
   * Регистрация провайдера
   *
   * @param {Number} providerID Идентификатор провайдера
   * @param {Object} provider Инстанс провайдера
   * */
  registerProvider (providerID, provider) {
    this.providers[providerID] = provider
  }

  /**
   * Дерегистрация провайдера
   *
   * @param {Number} providerID Идентификатор провайдера
   * */
  unregisterProvider (providerID) {
    delete this.providers[providerID]
  }

  /**
   * Возвращает провайдера
   *
   * @param {Number} providerID Идентификатор провайдера
   * */
  getProvider (providerID) {
    return this.providers[providerID]
  }

  /**
   * Регистрация клиента VRack
   *
   * @param {Number} providerID Идентификатор провайдера
   * @param {Number} clientID Идентификатор клиента
   * @param {Object} meta Дополнительные данные регистрации
  */
  registerClient (providerID, clientID, meta) {
    const data = {
      authorize: false,
      cipher: false,
      key: false,
      private: false,
      level: 1000,
      meta
    }
    this.clients.set(providerID + '.' + clientID, data)
    this.SystemEvents.emit('Guard.register', data)
  }

  /**
   * Дерегистрация клиента VRack
   *
   * @param {Number} providerID Идентификатор провайдера
   * @param {Number} clientID Идентификатор клиента
  */
  unregisterClient (providerID, clientID) {
    this.SystemEvents.emit('Guard.unregister', this.clients.get(providerID + '.' + clientID))
    if (this.clients.has(providerID + '.' + clientID)) return this.clients.delete(providerID + '.' + clientID)
  }

  /**
   * Возвращает данные клиента VRack
   *
   * @param {Number} providerID Идентификатор провайдера
   * @param {Number} clientID Идентификатор клиента
  */
  getClient (providerID, clientID) {
    if (this.clients.has(providerID + '.' + clientID)) return this.clients.get(providerID + '.' + clientID)
    return null
  }
}
