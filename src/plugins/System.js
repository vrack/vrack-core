const BasicPlagin = require('./BasicPlugin')
const Loader = require('vrack-loader')
const VRackError = require('../errors/Error')

module.exports = class extends BasicPlagin {
  checkParams () {
    return []
  }

  init () {
    this.SystemEvents = Loader.get('SystemEvents')
  }

  process () {
    process.on('uncaughtException', this.sysError.bind(this))
  }

  sysError (error) {
    console.log(error)
    const nError = (new VRackError()).import(error)
    this.SystemEvents.emit('System.error', nError.export())
  }
}
