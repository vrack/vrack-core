const Loader = require('vrack-loader')
const VRackError = require('../errors/Error')
const { parentPort, workerData } = require('worker_threads')
/**
 * Провайдер для получения команд с хозяина воркера и отправкой результатов
 * */
module.exports = class {
  constructor () {
    this.Master = {}
    this.Queue = new Map()
    this.index = 1000
  }

  init () {
    // Подписываемся на лоадер
    Loader.setListener(this.loaderListener.bind(this))
    process.addListener('uncaughtException', (error) => {
      error.action = 'uncaught'
      this.terminate(error)
    })
  }

  process () {
    this.Master = Loader.get('Master')
    parentPort.on('message', async (command) => {
      if (command.internal) {
        if (this.Queue.has(command.__iIndex)) {
          const promise = this.Queue.get(command.__iIndex)
          this.Queue.delete(command.__iIndex)
          if (command.result === 'success') {
            promise.resolve(command.resultData)
          } else if (command.result === 'error') {
            promise.reject(new VRackError('').import(command.resultData))
          }
        }
        return
      }

      try {
        const result = await this.Master.command(command, command.client)
        command.result = 'success'
        command.resultData = result
        parentPort.postMessage(command)
      } catch (err) {
        command.result = 'error'
        command.resultData = VRackError.objectify(err)
        parentPort.postMessage(command)
      }
    })
  }

  /**
   * Позволяет выоплнить команду DashboardManager
   *
  */
  request (command, data) {
    return new Promise((resolve, reject) => {
      const index = this.#queueIndex()
      this.Queue.set(index, { resolve, reject })
      const ncmd = {
        command: command,
        data,
        __iIndex: index,
        internal: true
      }
      parentPort.postMessage(ncmd)
    })
  }

  // Получение нового индекса сообщения
  #queueIndex () {
    return ++this.index
  }

  /**
   * Отправляет сообщение в WorkerManager
   *
   * @param {Object} message данные для отправки
  */
  postMessage (message) {
    parentPort.postMessage(message)
  }

  /**
   * Отправляет ошибку в WorkerManager
   *
   * @param {Error} error ошибка для отправки перед завершением процесска
  */
  postError (error) {
    this.postMessage({
      command: 'error',
      threadId: require('worker_threads').threadId,
      error: VRackError.objectify(error)
    })
  }

  /**
   * Отправляет ошибку и завершает работу процесса
   *
   * @param {Error} error ошибка для отправки перед завершением процесска
  */
  terminate (error) {
    this.postError(error)
    process.exit()
  }

  /**
   * Отправка бродкаста в WorkerManager
   *
   * @param {String} channel Канал для отправки бродкаста
   * @param {String} message Сообщение бродкаста
   * @param {String} data Данные бродкаста
  */
  broadcast (channel, data) {
    this.postMessage({
      command: 'broadcast',
      channel: channel,
      data: data
    })
  }

  /**
   * Листенер для заргузчика, ожидает завершение загрузки и отправляет
   * специальное сообщение WorkerManager для определения окончания загрузки воркера
   *
   * @param {String} status Статус сообщения
   * @param {String} message Cообщение
  */
  loaderListener (status, message) {
    if (status === 'event' && message === 'End loading') {
      this.postMessage({
        __index: workerData.__index,
        __id: workerData.__id,
        resultData: workerData.__id
      })
    }
  }
}
