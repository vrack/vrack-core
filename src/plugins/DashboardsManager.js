const fs = require('fs')
const path = require('path')
const Loader = require('vrack-loader')
const Rule = require('../helpers/validator/Rule')
const CommandError = require('../errors/CommandError')
const BasicPlagin = require('./BasicPlugin')
const VRackError = require('../errors/Error')
module.exports = class extends BasicPlagin {
  dashboardsList = {}
  dashboardsWorker = {}
  dashboardsErrors = {}
  dashboardsMeta = {}
  dashboardsGroupList = new Set()
  dashboardsGroup = {}
  dashboardDir = ''
  dashboardsErrorsIndex = 1000
  dashboardsLastError = {}
  dashboardsReloadTimer = {}

  systemEventsList = new Set() // Список ивентов на которые подписаны
  systemEventsListeners = new Map() // Списки дашбордов которые подписаны на ивенты

  __index = 1;
  _defaultMeta = {
    name: 'No name',
    group: 'Default',
    description: 'No description',
    private: false,
    autoStart: false,
    autoReload: false,
    systemEvents: [],
    version: '1.0'
  }

  checkParams () {
    return [
      new Rule('errorsCount').required().default(30).isNumber().expression('value > 0'),
      new Rule('dashboardDirs').required().default(['./dashboards']).isArray(),
      new Rule('dashboardConfig').required().default('./config.dashbaord.json').isFile(),
      new Rule('dashboardAutoStart').required().default(true).isBoolean(),
      new Rule('dashboardAutoReload').required().default(true).isBoolean()
    ]
  }

  init () {
    this.Master = Loader.get('Master')
    this.Guard = Loader.get('Guard')
    this.SystemEvents = Loader.get('SystemEvents')
    this.WorkersManager = Loader.get('WorkersManager')
    this.ApiKeyManager = Loader.get('ApiKeyManager')
    this.dashboardConfig = path.join(this.config.dashboardConfig)
    this.#updateList()
  }

  process () {
    this.Master.registerSubMaster(this.dashboardAction.bind(this))

    this.ApiKeyManager.registerFilter(
      'dashboards',
      'Dashboards filter',
      'Фильтрует доступ к конкретным дашбордам',
      function (command, filters) {
        if (filters.dashboards && filters.dashboards.length) return true
      },
      function (command, filters) {
        const commands = [
          'dashboardRun',
          'dashboardCheck',
          'dashboardStop',
          'dashboardStopReload',
          'dashboardErrors',
          'dashboardErrorsClear',
          'dashboardStructureUpdate',
          'dashboardDeviceAction',
          'dashboard',
          'dashboardMeta',
          'dashboardShares',
          'dashboardStructure'
        ]
        if (
          commands.indexOf(command.command) !== -1 &&
          filters.dashboards.indexOf(command.data.dashboard) === -1
        ) return true
        return false
      }
    )

    this.Master.registerCommand({
      command: 'dashboardRun',
      level: 1,
      rules: [new Rule('dashboard').required().isString()],
      operator: this.dashboardRun.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboardCheck',
      level: 1,
      rules: [new Rule('dashboard').required().isString()],
      operator: this.dashboardCheck.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboardStop',
      level: 1,
      rules: [new Rule('dashboard').required().isString()],
      operator: this.dashboardStop.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboardStopReload',
      level: 1,
      rules: [new Rule('dashboard').required().isString()],
      operator: this.dashboardStopReload.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboardErrors',
      level: 1,
      rules: [new Rule('dashboard').required().isString()],
      operator: this.dashboardErrors.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboardErrorsClear',
      level: 1,
      rules: [new Rule('dashboard').required().isString()],
      operator: this.dashboardErrorsClear.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboardListUpdate',
      level: 2,
      operator: this.dashboardListUpdate.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboard',
      level: 3,
      rules: [new Rule('dashboard').required().isString()],
      operator: this.dashboard.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboardList',
      level: 3,
      operator: this.dashboardList.bind(this)
    })

    this.Master.registerCommand({
      command: 'dashboardMeta',
      level: 3,
      rules: [new Rule('dashboard').required().isString()],
      operator: this.dashboardMeta.bind(this)
    })

    if (this.config.dashboardAutoStart) {
      for (const id in this.dashboardsMeta) {
        if (this.dashboardsMeta[id].autoStart) {
          this.dashboardRun({
            command: 'dashboardRun',
            data: { dashboard: id }
          }, { level: 1 })
            .catch((error) => {
              console.error(error)
            })
        }
      }
    }
  }

  async dashboardRun (command, client) {
    var id = command.data.dashboard
    if (this.dashboardsWorker[id]) throw new CommandError('Dashboard is already running')
    if (!this.dashboardsList[id]) throw new CommandError('Dashboard not found')
    this.dashboardsWorker[id] = await this.WorkersManager.addWorker({
      fileConfig: this.dashboardConfig,
      dashboard: this.dashboardsList[id],
      meta: this.dashboardsMeta[id]
    }, (error) => {
      // ON ERROR
      if (this.dashboardsErrors[id].length > this.config.errorsCount) this.dashboardsErrors[id].pop()
      this.#addError(id, error)
    }, () => {
      // ON EXIT
      this.dashboardsWorker[id] = false
      this.dashboardsList[id].run = false

      for (const event of this.dashboardsList[id].systemEvents) {
        this.systemEventsListeners.get(event).delete(id)
      }
      this.dashboardsList[id].systemEvents = []

      this.SystemEvents.emit('DashboardsManager.dashboard.exit', this.dashboardsList[id])
      if (
        this.dashboardsLastError[id] &&
        this.dashboardsLastError[id].action !== 'init' &&
        this.dashboardsLastError[id].action !== 'process' &&
        this.dashboardsLastError[id].action !== 'preprocess' &&
        this.dashboardsMeta[id].autoReload &&
        this.config.dashboardAutoReload
      ) {
        this.dashboardsList[id].reloadTimer = true
        this.dashboardsReloadTimer[id] = setTimeout(() => {
          this.dashboardsList[id].reloadTimer = false
          this.dashboardsReloadTimer[id] = false
          if (!this.dashboardsWorker[id]) this.dashboardRun(command, client)
        }, 5000)
      }
      this.#broadcastUpdate([id])
    })

    // START DASHBOARD
    await this.dashboardAction(command, client)
    this.dashboardsList[id].run = true
    this.dashboardsList[id].startedAt = Date.now()
    this.dashboardsList[id].systemEvents = this.dashboardsMeta[id].systemEvents
    this.SystemEvents.emit('DashboardsManager.dashboard.run', this.dashboardsList[id])
    this.#broadcastUpdate([id])

    if (!this.dashboardsList[id].systemEvents.length) return
    // SYSTEM EVENTS
    for (const event of this.dashboardsList[id].systemEvents) {
      if (!this.systemEventsList.has(event)) {
        this.systemEventsList.add(event)
        this.systemEventsListeners.set(event, new Set([id]))
        this.SystemEvents.on(event, (data) => {
          const dashSet = this.systemEventsListeners.get(event)
          for (const dash of dashSet.values()) {
            this.dashboardAction({
              command: 'dashboardSystemEvent',
              data: { dashboard: dash, event: event, data }
            }, { level: 1 })
          }
        })
      } else {
        this.systemEventsListeners.get(event).add(id)
      }
    }
  }

  async dashboardCheck (command, client) {
    var id = command.data.dashboard
    if (!this.dashboardsList[id]) throw new CommandError('Dashboard not found')
    const errors = []
    const wid = await this.WorkersManager.addWorker({
      fileConfig: this.dashboardConfig,
      dashboard: this.dashboardsList[id]
    }, (error) => {
      errors.push(VRackError.objectify(error))
    }, () => {})
    command.client = { level: client.level }
    try { await this.WorkersManager.request(wid, command) } catch (error) {}
    await this.WorkersManager.stopWorker(wid)
    return errors
  }

  async dashboardStop (command) {
    if (!this.dashboardsList[command.data.dashboard]) throw new CommandError('Dashboard not found')
    if (!this.dashboardsWorker[command.data.dashboard]) throw new CommandError('Dashboard not run')
    await this.WorkersManager.stopWorker(this.dashboardsWorker[command.data.dashboard])
    return ''
  }

  async dashboardErrors (command) {
    if (!this.dashboardsList[command.data.dashboard]) throw new CommandError('Dashboard not found')
    return this.dashboardsErrors[command.data.dashboard]
  }

  async dashboardErrorsClear (command) {
    if (!this.dashboardsList[command.data.dashboard]) throw new CommandError('Dashboard not found')
    this.dashboardsErrors[command.data.dashboard] = []
    this.dashboardsList[command.data.dashboard].errors = 0
    this.dashboardsList[command.data.dashboard].lastError = 0
    this.#broadcastUpdate([command.data.dashboard])
    return ''
  }

  async dashboardStopReload (command) {
    if (this.dashboardsWorker[command.data.dashboard]) throw new CommandError('Dashboard is already running')
    if (!this.dashboardsList[command.data.dashboard]) throw new CommandError('Dashboard not found')
    if (!this.dashboardsReloadTimer[command.data.dashboard]) throw new CommandError('Dashboard not found')
    clearTimeout(this.dashboardsReloadTimer[command.data.dashboard])
    this.dashboardsReloadTimer[command.data.dashboard] = false
    this.dashboardsList[command.data.dashboard].reloadTimer = false
    return ''
  }

  async dashboardListUpdate (command, client) {
    this.#updateList()
    this.#broadcastUpdate(Object.keys(this.dashboardsList))
    return this.#dashboardsList(client)
  }

  async dashboard (command) {
    if (!command.data.dashboard) throw new CommandError('Command dashboard must contain dashboard')
    var id = command.data.dashboard
    if (!this.dashboardsList[id]) throw new CommandError('Dashboard not found')
    return this.dashboardsList[id]
  }

  async dashboardList (command, client) {
    return this.#dashboardsList(client)
  }

  async dashboardMeta (command) {
    if (!this.dashboardsList[command.data.dashboard]) throw new CommandError('Dashboard not found')
    return this.dashboardsMeta[command.data.dashboard]
  }

  async dashboardAction (data, client) {
    if (!this.dashboardsList[data.data.dashboard]) throw new CommandError('Dashboard not found')
    if (!this.dashboardsWorker[data.data.dashboard]) throw new CommandError('Dashboard not run')
    data.client = { level: client.level }
    return await this.WorkersManager.request(this.dashboardsWorker[data.data.dashboard], data)
  }

  #dashboardsList = function (client) {
    if (
      client.key.filters && (
        !client.key.filters.dashboards ||
        !client.key.filters.dashboards.length
      )
    ) return this.dashboardsList
    const res = {}
    for (const dn of client.key.filters.dashboards) {
      if (this.dashboardsList[dn]) res[dn] = this.dashboardsList[dn]
    }
    return res
  }

  #broadcastUpdate = function (dashboards) {
    for (var dashboard of dashboards) {
      this.Guard.broadcast({
        command: 'broadcast',
        channel: 'manager.' + dashboard,
        data: this.dashboardsList[dashboard]
      })
    }
  }

  #updateList = () => {
    for (var dir of this.config.dashboardDirs) {
      dir = path.join(dir)
      this.#updateDashboardDir(dir)
    }
    for (var key in this.dashboardsList) {
      if (!fs.existsSync(this.dashboardsList[key].path)) {
        if (!this.dashboardsWorker[key]) {
          delete this.dashboardsList[key]
        } else {
          this.dashboardsList[key].deleted = true
        }
      }
    }
  }

  #addError = (id, error) => {
    this.dashboardsLastError[id] = error
    error.dashboard = id
    error.id = this.dashboardsErrorsIndex
    error.timestamp = Date.now()
    this.dashboardsErrorsIndex++
    this.dashboardsErrors[id].unshift(VRackError.objectify(error))
    this.dashboardsList[id].errors++
    this.SystemEvents.emit('DashboardsManager.dashboard.error', VRackError.objectify(error))
  }

  #readDashboardMeta = (id, dir) => {
    var metaPath = path.join(dir, id + '.meta.json')
    if (!fs.existsSync(metaPath)) return Object.assign({}, this._defaultMeta)
    if (fs.statSync(metaPath).isDirectory()) return Object.assign({}, this._defaultMeta)
    try {
      var mContent = fs.readFileSync(metaPath)
      var metaContent = JSON.parse(mContent)
      return Object.assign({ ...this._defaultMeta }, metaContent)
    } catch (error) {
      this.#addError(id, error)
      return this._defaultMeta
    }
  }

  #getFilesList = (dir) => {
    const files = fs.readdirSync(dir)
    const result = []
    for (var i in files) {
      var fpath = path.join(dir, files[i])
      if (!fs.statSync(fpath).isDirectory()) result.push(files[i])
    }
    result.sort()
    return result
  }

  #updateDashboardDir = (dir) => {
    if (!fs.existsSync(dir)) throw new Error('Dashboards dir not found')
    const fileList = this.#getFilesList(dir)
    for (var filename of fileList) {
      const expl = filename.split('.')
      if (expl.length !== 2) continue
      if (expl[1] !== 'json') continue
      const dbID = expl[0]
      this.dashboardsMeta[dbID] = this.#readDashboardMeta(dbID, dir)
      this.dashboardsGroupList.add(this.dashboardsMeta[dbID].group)
      if (!this.dashboardsGroup[this.dashboardsMeta[dbID].group]) this.dashboardsGroup[this.dashboardsMeta[dbID].group] = []
      this.dashboardsGroup[this.dashboardsMeta[dbID].group].push(dbID)

      if (!this.dashboardsList[dbID]) {
        this.dashboardsErrors[dbID] = []
        this.dashboardsList[dbID] = {
          id: dbID,
          name: this.dashboardsMeta[dbID].name,
          group: this.dashboardsMeta[dbID].group,
          run: false,
          path: path.join(dir, filename),
          structurePath: path.join(dir, dbID + '.struct.json'),
          metaPath: path.join(dir, dbID + '.meta.json'),
          configPath: path.join(dir, dbID + '.conf.json'),
          errors: 0,
          deleted: false,
          systemEvents: []
        }
      } else {
        this.dashboardsList[dbID].name = this.dashboardsMeta[dbID].name
        this.dashboardsList[dbID].group = this.dashboardsMeta[dbID].group
        this.dashboardsList[dbID].deleted = false
      }
    }
  }
}
