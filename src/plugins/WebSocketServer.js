const WebSocket = require('ws')
const Loader = require('vrack-loader')

module.exports = class {
  websocketIndex = 1
  websocketList = new Map()
  pingSI = false;
  providerID = 'WebSocketServer'

  list = {
    tree: {},
    dashboards: {}
  }

  noop () { }

  process () {
    this.Guard = Loader.get('Guard')
    this.Guard.registerProvider(this.providerID, this)
    this.createServer()
  }

  createServer () {
    const wss = new WebSocket.Server(this.config.server)
    wss.on('connection', (ws, req) => {
      ws.id = this.websocketIndex
      ws.isAlive = true
      this.websocketIndex++
      this.websocketList.set(ws.id, ws)
      this.Guard.registerClient(this.providerID, ws.id, { ip: req.socket.remoteAddress })
      ws.on('pong', () => { ws.isAlive = true })
      ws.on('message', (data) => this.message(ws, data))
      ws.on('close', () => {
        this.Guard.unregisterClient(this.providerID, ws.id)
        this.websocketList.delete(ws.id)
      })
    })

    wss.on('error', function (error) {
      console.error('Error websocket server')
      console.error(error)
    })

    this.pingSI = setInterval(() => {
      wss.clients.forEach((ws) => {
        if (ws.isAlive === false) {
          this.websocketList.delete(ws.id)
          return ws.terminate()
        }
        ws.isAlive = false
        ws.ping(() => {})
      })
    }, 15000)
  }

  async message (ws, data) {
    const res = await this.Guard.receive(this.providerID, ws.id, data)
    this.send(ws, res)
  }

  clientSend (clientID, data) {
    if (!this.websocketList.has(clientID)) return
    var client = this.websocketList.get(clientID)
    this.send(client, data)
  }

  send (client, data) {
    if (client.readyState === WebSocket.OPEN) client.send(data)
    else this.websocketList.delete(client.id)
  }
}
