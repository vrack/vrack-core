const Loader = require('vrack-loader')
const Rule = require('../helpers/validator/Rule')

module.exports = class {
  _channels = new Map()
  _entities = new Map()
  _clients = new Map()

  init () {
    this.Master = Loader.get('Master')
  }

  process () {
    this.Master.registerCommand({
      command: 'channelJoin',
      level: 3,
      rules: [
        new Rule('channel').required().isString(),
        new Rule('filter').required().default([]).isArray()
      ],
      operator: this.channelJoin.bind(this)
    })

    this.Master.registerCommand({
      command: 'channelLeave',
      level: 3,
      rules: [
        new Rule('channel').required().isString()
      ],
      operator: this.channelLeave.bind(this)
    })

    this.Master.registerCommand({
      command: 'channelLeaveAll',
      level: 3,
      operator: this.channelLeaveAll.bind(this)
    })

    this.Master.registerCommand({
      command: 'channelsLeave',
      level: 3,
      rules: [
        new Rule('channels').required().isArray()
      ],
      operator: this.channelsLeave.bind(this)
    })
  }

  async channelJoin (command) {
    var entity = this._createCommandEntity(command.providerID, command.clientID)
    var result = []
    if (command.data.filter.length) {
      for (var filter of command.data.filter) {
        var channel = [command.data.channel, filter].join('.')
        this._getChannel(channel, true).add(entity)
        this._getEntity(entity, true).add(channel)
        result.push(channel)
      }
      return result
    }
    this._getChannel(command.data.channel, true).add(entity)
    this._getEntity(entity, true).add(command.data.channel)
    result.push(command.data.channel)
    return result
  }

  async channelLeave (command) {
    var entity = this._createCommandEntity(command.providerID, command.clientID)
    this._getChannel(command.data.channel, true).delete(entity)
    this._getEntity(entity, true).delete(command.data.channel)
    return ''
  }

  async channelsLeave (command) {
    var entity = this._createCommandEntity(command.providerID, command.clientID)
    for (var channel of command.data.channels) {
      this._getChannel(channel, true).delete(entity)
      this._getEntity(entity, true).delete(channel)
    }
    return ''
  }

  async channelLeaveAll (command) {
    return this.clientClear(command.providerID, command.clientID)
  }

  clientClear (providerID, clientID) {
    var entity = this._createCommandEntity(providerID, clientID)
    var channels = this._getEntity(entity, true)
    var result = []
    for (const channel of channels.values()) {
      result.push(channel)
      this._getChannel(channel, true).delete(entity)
    }
    this._entities.delete(entity)
    return result
  }

  broadcast (channel, cb) {
    const ch = this._getChannel(channel, true)
    for (const value of ch) {
      const client = this._clients.get(value)
      cb(client.providerID, client.clientID)
    }
  }

  hasClients (channel) {
    const ch = this._getChannel(channel)
    if (!ch || !ch.size) return false
    return true
  }

  _getChannel (channel, create = false) {
    if (create && !this._channels.has(channel)) this._channels.set(channel, new Set())
    return this._channels.get(channel)
  }

  _getEntity (entity, create = false) {
    if (create && !this._entities.has(entity)) this._entities.set(entity, new Set())
    return this._entities.get(entity)
  }

  _createCommandEntity (providerID, clientID) {
    var entity = providerID + '.' + clientID
    if (!this._clients.has(entity)) {
      this._clients.set(entity, { providerID, clientID })
    }
    return entity
  }
}
