const Loader = require('vrack-loader')
const path = require('path')

module.exports = class {
  init () {
    Loader.get('DashboardsWatcher').registerConverter('js', this.convert.bind(this))
  }

  convert (dashboard, filename) {
    try {
      return this.requireUncached(path.join(dashboard, filename))
    } catch (error) {
      console.log(error)
      return false
    }
  }

  requireUncached (module) {
    delete require.cache[require.resolve(module)]
    return require(module)
  }
}
